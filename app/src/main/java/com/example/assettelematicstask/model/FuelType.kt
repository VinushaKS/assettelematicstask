package com.example.assettelematicstask.model

data class FuelType(
    val images: String?= null,
    val text: String?= null,
    val value: Int =0
)