package com.example.assettelematicstask.model

data class VehicleMake(
    val images: String?=null,
    val text: String?=null,
    val value: Int=0
)