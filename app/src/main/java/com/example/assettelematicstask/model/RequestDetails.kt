package com.example.assettelematicstask.model

data class RequestDetails(
    val clientid: Int = 0,
     val enterprise_code: Int = 0,
     val passcode: Int = 0,
     val mno: String? = null
)