package com.example.assettelematicstask.model

data class ApiError(
    val message:String?=null,
    val status :Int =0
)
