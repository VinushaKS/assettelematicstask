package com.example.assettelematicstask.model


data class Vehicle(
    val fuel_type: List<FuelType>?= emptyList(),
    val manufacture_year: List<ManufactureYear>?= emptyList(),
    val message: String?=null,
    val status: Int=0,
    val vehicle_capacity: List<VehicleCapacity> ?= emptyList(),
    val vehicle_make: List<VehicleMake>?= emptyList(),
    val vehicle_type: List<VehicleType>?= emptyList()
)