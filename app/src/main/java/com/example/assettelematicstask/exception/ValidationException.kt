package com.example.assettelematicstask.exception

import androidx.annotation.StringRes

open class ValidationException(@StringRes val errorMes: Int) : RuntimeException()
