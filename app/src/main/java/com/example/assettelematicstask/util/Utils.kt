package com.example.assettelematicstask.util

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast


object Utils {
    // Network Check
    fun isOnline(context: Context):Boolean {
       val isNetworkConnected = try {
            val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
           val activeNetwork = cm.activeNetworkInfo
            null != activeNetwork
        } catch (e: Exception) {
            false
        }
        return isNetworkConnected
    }

    fun displayToast(context: Context,message: String){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show()
    }
}