package com.example.assettelematicstask.repository

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.assettelematicstask.model.ApiError
import com.example.assettelematicstask.model.RequestDetails
import com.example.assettelematicstask.model.Vehicle
import com.example.assettelematicstask.retrofit.service.ApiService
import com.example.assettelematicstask.retrofit.service.RetrofitInstance
import com.example.assettelematicstask.util.Utils.isOnline
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class VehicleRepository (private val context: Context) {


    /**
     * Get Vehicle details
     */
    fun getVehicleDetails(
        requestDetails: RequestDetails,
        success: (Vehicle) -> Unit,
        error: (ApiError) -> Unit
    ) {
       if (!isOnline(context)) {
           error.invoke(ApiError("No internet"))
       }
       val retrofitInstance= RetrofitInstance.getRetrofitInstance().create(ApiService::class.java)
       val call = retrofitInstance.postVehicleDetails(requestDetails)
       call.enqueue(object : Callback<Vehicle> {
           override fun onResponse(call: Call<Vehicle>, response: Response<Vehicle>) {
               if (response.isSuccessful) {
                   response.body()?.let { success.invoke(it) }
               }
           }

           override fun onFailure(call: Call<Vehicle>, t: Throwable) {
               error.invoke(ApiError(t.localizedMessage))
           }
       })
   }
}