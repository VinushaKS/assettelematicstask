package com.example.assettelematicstask.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.assettelematicstask.R
import com.example.assettelematicstask.exception.ValidationException
import com.example.assettelematicstask.model.*
import com.example.assettelematicstask.repository.VehicleRepository

class VehicleViewModel(private val repository: VehicleRepository) : ViewModel() {

    // LiveData for Vehicle
     val listOfVehicle = MutableLiveData<Vehicle>()
     val vehicleNames = MutableLiveData<List<String>>()
     val vehicleMake = MutableLiveData<List<String>>()
     val vehicleFuel = MutableLiveData<List<String>>()
     val vehicleCapacity = MutableLiveData<List<String>>()
     val vehicleYear = MutableLiveData<List<String>>()

    fun getVehicleDetails(
        success: (Vehicle) -> Unit,
        error: (ApiError) -> Unit
    ) {
        val requestDetails = RequestDetails(11,1007,3476,"9889897789")
       /*  if (requestDetails.clientId?.isBlank() == true) {
             throw ValidationException(R.string.clientIdNull)
         } else if (requestDetails.enterprise_code?.isBlank()==true) {
             throw ValidationException(R.string.enterPriceNull)
         } else if (requestDetails.passCode?.isBlank() == true) {
             throw ValidationException(R.string.passCodeNull)
         } else*/
        if (requestDetails.mno?.isBlank() == true) {
             throw ValidationException(R.string.MobileNumberNull)
         }
        repository.getVehicleDetails(
            requestDetails,
            success = {
                success.invoke(it)
                listOfVehicle.value = it
                vehicleNames.value =  it.vehicle_type?.map {it1-> it1.text.toString() }
                vehicleMake.value =  it.vehicle_make?.map {it1-> it1.text.toString() }
                vehicleFuel.value =  it.fuel_type?.map {it1-> it1.text.toString() }
                vehicleCapacity.value =  it.vehicle_capacity?.map {it1-> it1.text.toString() }
                vehicleYear.value =  it.manufacture_year?.map {it1-> it1.text.toString() }

            },
            error = {
                error.invoke(it)
            })
    }
}