package com.example.assettelematicstask.retrofit.service

import com.example.assettelematicstask.model.RequestDetails
import com.example.assettelematicstask.model.Vehicle
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {

    @POST("mobile/configure/v1/task")
    fun postVehicleDetails(
        @Body requestDetails: RequestDetails
    ): Call<Vehicle>
}