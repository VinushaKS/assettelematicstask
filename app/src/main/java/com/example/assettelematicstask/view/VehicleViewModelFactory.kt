package com.example.assettelematicstask.view

import android.annotation.SuppressLint
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.assettelematicstask.repository.VehicleRepository
import com.example.assettelematicstask.viewModel.VehicleViewModel

class VehicleViewModelFactory(private val repository: VehicleRepository): ViewModelProvider.Factory {

    @SuppressLint("SuspiciousIndentation")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VehicleViewModel::class.java))
        return VehicleViewModel(repository) as T
        throw java.lang.IllegalArgumentException("unKnow error")

    }
}