package com.example.assettelematicstask.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.assettelematicstask.R
import com.example.assettelematicstask.databinding.ListItemBinding
import com.example.assettelematicstask.model.VehicleType

class MyAdapter :
    RecyclerView.Adapter<MyViewHolder>() {
    private val vehicleTypeList = ArrayList<VehicleType>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ListItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.list_item, parent, false)
        return MyViewHolder(binding)

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(vehicleTypeList[position])

    }

    override fun getItemCount(): Int {
        return vehicleTypeList.size
    }

    fun setList(list: List<VehicleType>) {
        vehicleTypeList.clear()
        vehicleTypeList.addAll(list)
        notifyDataSetChanged()
    }
}

class MyViewHolder(private val binding: ListItemBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(vehicleType: VehicleType) {
        binding.textViewVehicleType.text = vehicleType.text
       Glide.with(this.itemView)
           .load("")
           .placeholder(R.drawable.androidpicture)
           .fallback(R.drawable.androidpicture)
           .error(R.drawable.androidpicture)
           .into(binding.imageViewVehicleType)
    }

}
