package com.example.assettelematicstask.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.assettelematicstask.view.adapter.MyAdapter
import com.example.assettelematicstask.R
import com.example.assettelematicstask.databinding.ActivityVehicleBinding
import com.example.assettelematicstask.model.Vehicle
import com.example.assettelematicstask.model.VehicleType
import com.example.assettelematicstask.repository.VehicleRepository
import com.example.assettelematicstask.util.Utils.displayToast
import com.example.assettelematicstask.viewModel.VehicleViewModel
import com.google.zxing.integration.android.IntentIntegrator


class VehicleActivity : AppCompatActivity() {

    private lateinit var binding: ActivityVehicleBinding
    lateinit var vehicleViewModel: VehicleViewModel
    lateinit var myAdapter: MyAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_vehicle)
        setContentView(binding.root)
        val repository = VehicleRepository(this)

        val factory = VehicleViewModelFactory(repository)
        vehicleViewModel =
            ViewModelProvider(this,factory)[VehicleViewModel::class.java]
        getVehicleDetails()
        binding.cardView.setOnClickListener {
            val intentIntegrator = IntentIntegrator(this)
            intentIntegrator.setPrompt(getString(R.string.qrBar))
            intentIntegrator.setOrientationLocked(true)
            intentIntegrator.setBarcodeImageEnabled(true)
            intentIntegrator.initiateScan()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_sync) {
            Toast.makeText(this@VehicleActivity, "Action clicked", Toast.LENGTH_LONG).show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * To get the Vehicle details
     * Given all the input as static input
     */
   private fun getVehicleDetails() {
        vehicleViewModel.getVehicleDetails( success = {
            it.message?.let { it1 ->
                displayToast(this, it1)
                setSpinner()
                it.vehicle_type?.let { it2 -> setRecycler(it2) }
            }
        }, error = {
            it.message?.let { it1 -> displayToast(this, it1) }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents == null) {
                Toast.makeText(baseContext, "Cancelled", Toast.LENGTH_SHORT).show()
            } else {
              Log.i("TAG",intentResult.toString())
                binding.textImei.text = intentResult.contents.toString()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    /**
     *  To set adapter to the Spinner
     */
    private fun setAdapter(value: List<String>,adapterView: Spinner){
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, value)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        adapterView.adapter = adapter
    }

    /**
     * RecyclerView to set vehicle type
     */
    private fun setRecycler(list:List<VehicleType>){
        binding.recyclerViewVehicle.apply {
            this.layoutManager = LinearLayoutManager(this@VehicleActivity,LinearLayoutManager.HORIZONTAL,false)
            myAdapter = MyAdapter()
            myAdapter.setList(list)
            adapter =myAdapter
        }
    }

    /**
     * Map values to the spinner
     */
    private fun setSpinner(){
        vehicleViewModel.vehicleNames.observe(this) { value ->
            setAdapter(value,binding.spinnerVehicle)
        }
        vehicleViewModel.vehicleMake.observe(this) { value ->
            setAdapter(value,binding.spinnerVehicleModel)
        }
        vehicleViewModel.vehicleFuel.observe(this) { value ->
            setAdapter(value,binding.spinnerFuel)
        }
        vehicleViewModel.vehicleCapacity.observe(this) { value ->
            setAdapter(value,binding.spinnerQty)
        }
        vehicleViewModel.vehicleYear.observe(this) { value ->
            setAdapter(value,binding.spinnerYear)
        }
        vehicleViewModel.vehicleMake.observe(this){value->
            setAdapter(value,binding.spinnerDriver)
        }
    }
}